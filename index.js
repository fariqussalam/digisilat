var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ip = require("ip");
const TurnamenModul = require('./modul/turnamen.js');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
return res.sendFile(__dirname + '/index.html');
});

app.get('/tanding/display', function(req, res) {
    return res.sendFile(__dirname + '/TandingDisplay.html');
});
app.get('/tanding/juri', function(req, res) {
    return res.sendFile(__dirname + '/TandingJuri.html');
});
app.get('/tanding/admin', function(req, res) {
    return res.sendFile(__dirname + '/AdminDisplay.html');
});
app.get('/tanding/timer', function(req, res) {
    return res.sendFile(__dirname + '/timerTanding.html');
});
app.get('/tanding/control', function(req, res) {
    return res.sendFile(__dirname + '/TandingControl.html');
});
app.get('/tgr/tunggal/display', function(req, res) {
    return res.sendFile(__dirname + '/DisplayTunggal.html');
});
app.get('/tgr/tunggal/admin', function(req, res) {
    return res.sendFile(__dirname + '/AdminTunggal.html');
});

app.get('/tgr/timer', function(req, res) {
    return res.sendFile(__dirname + '/timerTGR.html');
});

app.get('/tgr/ganda/display', function(req, res) {
    return res.sendFile(__dirname + '/DisplayGanda.html');
});
app.get('/tgr/regu/display', function(req, res) {
    return res.sendFile(__dirname + '/DisplayRegu.html');
});
app.get('/tgr/tunggal/juri', function(req, res) {
    return res.sendFile(__dirname + '/JuriTunggal.html');
});
app.get('/tgr/tunggal/timer', function(req, res) {
    return res.sendFile(__dirname + '/timerTunggal.html');
});
app.get('/tgr/ganda/timer', function(req, res) {
    return res.sendFile(__dirname + '/timerGanda.html');
});
app.get('/tgr/regu/timer', function(req, res) {
    return res.sendFile(__dirname + '/timerRegu.html');
});
app.get('/tgr/tunggal/control', function(req, res) {
    return res.sendFile(__dirname + '/controlTunggal.html');
});
app.get('/tgr/ganda/control', function(req, res) {
    return res.sendFile(__dirname + '/controlGanda.html');
});
app.get('/tgr/regu/control', function(req, res) {
    return res.sendFile(__dirname + '/controlRegu.html');
});
app.get('/tgr/ganda/juri', function(req, res) {
    return res.sendFile(__dirname + '/JuriGanda.html');
});
app.get('/tgr/regu/juri', function(req, res) {
    return res.sendFile(__dirname + '/JuriRegu.html');
});

app.get('/tgr/ganda/admin', function(req, res) {
    return res.sendFile(__dirname + '/AdminGanda.html');
});

app.get('/tgr/regu/admin', function(req, res) {
    return res.sendFile(__dirname + '/AdminRegu.html');
});

app.get('/setup', function(req, res) {
    return res.sendFile(__dirname + '/setup.html');
});

io.on('connection', function(socket) {
    socket.on('koneksi', function(data) {
        console.log(data.name + " Berhasil Terkoneksi");
        if (data.reqIp) {
            io.sockets.emit('res-ip-server', ip.address());
        }
    });
    socket.on('timer_set', function(data) {
        console.log("Waktu Timer Di Set : " +
            data + " Detik");
        var dataz = data;
        io.sockets.emit('display_setTimer', dataz);
    });
    socket.on('control_timer', function(data) {
        console.log("Timer : " + data);
        var dataz = data;
        io.sockets.emit('display_ControlTimer', dataz);
    });
    socket.on('detilkoneksijuri', function(data) {
        console.log("Nomor Juri : " +
            data.nomorJuri + " Terkoneksi");
    });
    socket.on('inputSkor', function(data) {
        console.log('Juri ' + data.nJuri + ' : Memasukkan ' + data.warna + " Nilai " +
            data.nilai + " Pada Ronde " + data.ronde + " indikator : " + data.indikator);
        var datax = data;
        io.sockets.emit('display_inputSkor', datax);
    });
    socket.on('hapusSkor', function(data) {
        console.log('Juri ' + data.nJuri + ' : Memasukkan ' + data.warna + " Nilai Hapus" +
            " Pada Ronde " + data.ronde + " indikator : " + data.indikator);
        var datax = data;
        io.sockets.emit('display_hapusSkor', datax);
    });
    socket.on('nextRonde', function(data) {
        console.log("Ronde Sekarang : " + data);
        var dataz = data;
        io.sockets.emit('roundControl', dataz);
    });
    socket.on('backRonde', function(data) {
        console.log("Ronde Sekarang : " + data);
        var dataz = data;
        io.sockets.emit('roundControl', dataz);
    });
    socket.on('detilPertandingan', function(data) {
        console.log("detil Pertandingan Telah Terkirim");
        io.sockets.emit('detil_pertandingan', data);
    });
    socket.on('pengumuman', function(data) {
        console.log("pengumuman Pemenang : " + data);
        var dataz = data;
        io.sockets.emit('pengumumanPemenang', dataz);
    });
    socket.on('detilKemenangan', function(data) {
        console.log("pengumuman Pemenang : " + data.poin);
        var dataz = data;
        io.sockets.emit('pemenang', dataz);
    });
    socket.on('disconnect', function() {
        console.log("Ada Koneksi Device yang Terputus");
    });

    socket.on('refreshTanding', function(data) {
        io.sockets.emit('doRefreshTanding');
    });

    socket.on('isiDataTurnamen', function() {
        let turnamen = TurnamenModul.getTurnamen();
        io.sockets.emit('terimaDataTurnamen', turnamen);
    });

    socket.on('simpan-data-turnamen', function(data) {
        let turnamen = TurnamenModul.setTurnamen(data);
       io.sockets.emit('terimaDataTurnamen', turnamen); 
    });

    socket.on('get_data_turnamen', function(){
        var turnamen = TurnamenModul.getTurnamen();
        io.sockets.emit('terimaDataTurnamen', turnamen);
    });
});



var tunggal = io.of('/tunggal');
tunggal.on('connection', function(socket) {
    console.log('Device TGR Tunggal Terkoneksi');

    socket.on('koneksi', function(data) {
        console.log(data.name + " Berhasil Terkoneksi");
    });
    socket.on('detilTunggal', function(data) {
        console.log("detil Tunggal Telah Dikirim");
        var dataz = data;
        tunggal.emit('display_detilTunggal', dataz);
    });
    socket.on('hapus_hukuman', function(data) {
        console.log("Hukuman Dihapus");
        var dataz = data;
        tunggal.emit('display_hapusHukuman', dataz);
    });
    socket.on('timer_set', function(data) {
        console.log("Waktu Timer Di Set : " +
            data + " Detik");
        var dataz = data;
        tunggal.emit('display_setTimer', dataz);
    });
    socket.on('control_timer', function(data) {
        console.log("Timer : " + data);
        var dataz = data;
        tunggal.emit('display_ControlTimer', dataz);
    });
    socket.on('pengumuman', function(data) {
        console.log("Pengumuman Skor Tunggal ");
        tunggal.emit('display_pengumuman', data);
    });
    socket.on('detilkoneksijuri', function(data) {
        console.log("Juri Tunggal Nomor " + data + " Berhasil Terkoneksi");
    });

    socket.on('inputSkorMinus', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan -1 Pada Jurus : ' + data.nJurus);
        var datax = data;
        tunggal.emit('display_inputSkorMinus', datax);
    });
    socket.on('inputSkorKemantapan', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai Kemantapan Sebesar : ' + data.nilai);
        var datax = data;
        tunggal.emit('display_inputSkorKemantapan', datax);
    });
    socket.on('inputSkorHukuman', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai Hukuman -5 dengan Kategori : ' + data.nKategori);
        var datax = data;
        tunggal.emit('display_inputSkorHukuman', datax);
    });
    socket.on('inputDis', function(data) {
        console.log('Juri ' + data + ' Mendiskualifikasi Peserta');
        var datax = data;
        tunggal.emit('display_inputDis', datax);
    });

    socket.on('refreshTunggal', function(data) {
        tunggal.emit('doRefreshTunggal');
    });

    socket.on('isiDataTurnamen', function() {
        let turnamen = TurnamenModul.getTurnamen();
        tunggal.emit('terimaDataTurnamen', turnamen);
    });

    socket.on('get_data_turnamen', function(){
        var turnamen = TurnamenModul.getTurnamen();
        tunggal.emit('terimaDataTurnamen', turnamen);
    });
});
var regu = io.of('/regu');
regu.on('connection', function(socket) {
    console.log('Device TGR Regu Terkoneksi');

    socket.on('koneksi', function(data) {
        console.log(data.name + " Berhasil Terkoneksi");
    });
    socket.on('timer_set', function(data) {
        console.log("Waktu Timer Di Set : " +
            data + " Detik");
        var dataz = data;
        regu.emit('display_setTimer', dataz);
    });
    socket.on('control_timer', function(data) {
        console.log("Timer : " + data);
        var dataz = data;
        regu.emit('display_ControlTimer', dataz);
    });
    socket.on('pengumuman', function(data) {
        console.log("Pengumuman Skor Regu ");
        regu.emit('display_pengumuman', data);
    });
    socket.on('detilkoneksijuri', function(data) {
        console.log("Juri Regu Nomor " + data + " Berhasil Terkoneksi");
    });

    socket.on('inputSkorMinus', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan -1 Pada Jurus : ' + data.nJurus);
        var datax = data;
        regu.emit('display_inputSkorMinus', datax);
    });
    socket.on('inputSkorKekompakan', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai Kekompakan sebesar : ' + data.nilai);
        var datax = data;
        regu.emit('display_inputSkorKekompakan', datax);
    });
    socket.on('inputSkorHukuman', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai Hukuman -5 dengan Kategori : ' + data.nKategori);
        var datax = data;
        regu.emit('display_inputSkorHukuman', datax);
    });
    socket.on('inputDis', function(data) {
        console.log('Juri ' + data + ' Mendiskualifikasi Peserta');
        var datax = data;
        regu.emit('display_inputDis', datax);
    });

    socket.on('refreshRegu', function(data) {
        regu.emit("doRefreshRegu");
    });

    socket.on('hapus_hukuman', function(data) {
        console.log("Hukuman Dihapus");
        var dataz = data;
        regu.emit('display_hapusHukuman', dataz);
    });

    socket.on('get_data_turnamen', function(){
        var turnamen = TurnamenModul.getTurnamen();
        regu.emit('terimaDataTurnamen', turnamen);
    });

     socket.on('dataPenampil', function(dataPenampil){
        regu.emit('dataPenampil', dataPenampil);
    });
});
var ganda = io.of('/ganda');
ganda.on('connection', function(socket) {
    console.log('Device TGR Ganda Terkoneksi');

    socket.on('koneksi', function(data) {
        console.log(data.name + " Berhasil Terkoneksi");
    });
    socket.on('timer_set', function(data) {
        console.log("Waktu Timer Di Set : " +
            data + " Detik");
        var dataz = data;
        ganda.emit('display_setTimer', dataz);
    });
    socket.on('control_timer', function(data) {
        console.log("Timer : " + data);
        var dataz = data;
        ganda.emit('display_ControlTimer', dataz);
    });
    socket.on('pengumuman', function(data) {
        console.log("Pengumuman Skor Ganda ");
        ganda.emit('display_pengumuman', data);
    });
    socket.on('detilkoneksijuri', function(data) {
        console.log("Juri Ganda Nomor " + data + " Berhasil Terkoneksi");
    });

    socket.on('inputSkorTSB', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai TSB Sebesar : ' + data.nilai);
        var datax = data;
        ganda.emit('display_inputSkorTSB', datax);
    });
    socket.on('inputSkorNKM', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai NKM Sebesar : ' + data.nilai);
        var datax = data;
        ganda.emit('display_inputSkorNKM', datax);
    });
    socket.on('inputSkorPGN', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai PGN Sebesar : ' + data.nilai);
        var datax = data;
        ganda.emit('display_inputSkorPGN', datax);
    });

    socket.on('inputSkorHukuman', function(data) {
        console.log('Juri ' + data.nJuri + ' Memasukkan Nilai Hukuman -5 dengan Kategori : ' + data.nKategori);
        var datax = data;
        ganda.emit('display_inputSkorHukuman', datax);
    });

    socket.on('inputDis', function(data) {
        console.log('Juri ' + data + ' Mendiskualifikasi Peserta');
        var datax = data;
        ganda.emit('display_inputDis', datax);
    });

    socket.on('refreshGanda', function(data) {
        ganda.emit("doRefreshGanda");
    });

    socket.on('hapus_hukuman', function(data) {
        console.log("Hukuman Dihapus");
        var dataz = data;
        ganda.emit('display_hapusHukuman', dataz);
    });

    socket.on('get_data_turnamen', function(){
        var turnamen = TurnamenModul.getTurnamen();
        ganda.emit('terimaDataTurnamen', turnamen);
    });

    socket.on('dataPenampil', function(dataPenampil){
        ganda.emit('dataPenampil', dataPenampil);
    });


});

http.listen(3000, function() {
    console.log('Server Telah Siap Pada Port 3000');
    console.log("IP Lokal Anda : " + ip.address());
});