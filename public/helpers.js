 // ------------------------------------------------------------------------------ //
 // Helpers
 // ------------------------------------------------------------------------------ //

 function simpanGambar(namaFile) {
     if (!namaFile) namaFile = "data_pertandingan_" + getFormattedDateTime();
     var printed = document.getElementById("printed");
     var container = document.getElementById("container");
     window.scrollTo(0, 0);
     html2canvas(printed, {
         dpi: 300
     }).then(function(canvas) {
         container.appendChild(canvas);
         var a = document.createElement('a');
         a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
         a.download = namaFile + '.jpg';
         a.click();
     }).then(function() {
         var canvas = document.getElementsByTagName('canvas');
         canvas[0].parentNode.removeChild(canvas[0]);
     });
 };

 function getFormattedDateTime() {
     var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth() + 1;
     var yyyy = today.getFullYear();
     var curr_hour = today.getHours();
     var curr_min = today.getMinutes();
     curr_min = curr_min + "";
     if (curr_min.length == 1) {
         curr_min = "0" + curr_min;
     }
     curr_hour = curr_hour + "";
     if (curr_hour.length == 1) {
         curr_hour = "0" + curr_hour;
     }

     if (dd < 10) {
         dd = '0' + dd
     }

     if (mm < 10) {
         mm = '0' + mm
     }
     var currentDate = yyyy + mm + dd + "_" + curr_hour + curr_min;
     return currentDate;
 };


 function getCurrentDate() {
     var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth() + 1;
     var yyyy = today.getFullYear();

     if (dd < 10) {
         dd = '0' + dd
     }

     if (mm < 10) {
         mm = '0' + mm
     }
     var currentDate = dd + '/' + mm + '/' + yyyy;
     return " " + currentDate;
 };

 function getCurrentTime() {
     var d = new Date();
     var curr_hour = d.getHours();
     var curr_min = d.getMinutes();
     curr_min = curr_min + "";
     if (curr_min.length == 1) {
         curr_min = "0" + curr_min;
     }
     return " " + curr_hour + ":" + curr_min
 };