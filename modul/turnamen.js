var fs = require('fs');
let Turnamen = {
	getTurnamen: function() {
		return setupTurnamen(bacaFileJSON('data_turnamen.json'));
	},
	setTurnamen: function(data) {
		if (!data.nama || !data.tempat || !data.waktu) return false;
		tulisFileJSON(data);
		return data;
	}
}

function bacaFileJSON(namafile) {
    if (!namafile) return {};
    try {
        let rawfile = fs.readFileSync(namafile);
        if (!rawfile) return {};
        return JSON.parse(rawfile);
    } catch (err) {
        return {};
    }
}

function tulisFileJSON(data) {
	let jsonStr = JSON.stringify(data, null, '\t');
	fs.writeFileSync('data_turnamen.json', jsonStr)
}

function setupTurnamen(turnamen) {
    let defaultTurnamen = {
        nama: "-",
        tempat: "-",
        waktu: "-"
    };
    if (!turnamen || !turnamen.nama) {
        turnamen = defaultTurnamen;
    }
    return turnamen;
}

module.exports = Turnamen;